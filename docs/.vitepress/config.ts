import { getKatas } from '../data/katas';
import { resolve } from 'path';

const katas = getKatas();

export default {
  katas,
  title: 'The secrects of Katas',
  description: 'To share the ideas of training katas on https://www.codewars.com/',
  themeConfig: {
    sidebar: {
      '/katas/': [
        {
          text: '按段位分类',
          children: [...Array(9)].map((_, i) => {
            return {
              text: i ? `${i} kyu` : 'beta',
              link: `/katas/kyu/${i ? `kyu${i}` : 'beta'}`,
              children: [],
            };
          }),
        },
      ],
    },
    nav: [
      {
        text: '首页',
        link: '/',
      },
      {
        text: 'Codewars',
        link: '/katas/',
      },
    ],
    lastUpdated: 'Last Updated',
  },
};
