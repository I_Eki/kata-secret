import DefaultTheme from 'vitepress/theme';
import { Quasar } from 'quasar';

// Import icon libraries
import '@quasar/extras/material-icons/material-icons.css';

// Import Quasar css
import 'quasar/src/css/index.sass';

import './styles/index.scss';

export default {
  ...DefaultTheme,
  enhanceApp({ app }) {
    app.use(Quasar, {}, { req: { headers: {} } });
  }
};
