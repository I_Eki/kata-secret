interface Kata {
  id: string;
  icon?: string;
  title: string;
  motoTitle?: string; // 原标题
  kyu: number;
  reporter?: string;
  reportedTime?: Date;
  reviewID?: string;
  coolSolutions?: string[]; // 优秀解决方案
  sakamotos?: string[]; // 已解决此题者
  comment?: string;
}

const enum Users {
  Eki = 'Eki',
  Cyberpunkjill = 'Cyberpunkjill',
}

export const getKatas = (): Kata[] => {
  return [
    {
      id: '613f13a48dfb5f0019bb3b0f',
      title: '淘汰赛 & 循环赛算法',
      motoTitle: '',
      kyu: 5,
      reporter: Users.Eki,
      sakamotos: [Users.Eki],
    },
    {
      id: '5a045fee46d843effa000070',
      title: '阶乘分解质因子',
      motoTitle: '',
      kyu: 5,
      reporter: Users.Eki,
      sakamotos: [Users.Eki],
    },
    {
      id: '52a382ee44408cea2500074c',
      title: '矩阵行列式',
      motoTitle: '',
      kyu: 4,
      reporter: Users.Cyberpunkjill,
    },
    {
      id: '521c2db8ddc89b9b7a0000c1',
      title: '蛇形数组',
      motoTitle: '',
      kyu: 4,
      reporter: Users.Cyberpunkjill,
      sakamotos: [Users.Eki],
    },
    {
      id: '5519a584a73e70fa570005f5',
      title: '用生成器来连续产生质数',
      motoTitle: '',
      kyu: 3,
      reporter: Users.Eki,
      reportedTime: new Date(2021, 10, 14, 16, 34),
      sakamotos: [Users.Eki],
    },
    {
      id: 'prime-streaming-nc-17',
      title: '用生成器来连续产生质数（2500万+/s）',
      motoTitle: '',
      kyu: 2,
      reporter: Users.Eki,
      reportedTime: new Date(2021, 10, 15, 17, 34),
    },
    {
      id: '534e01fbbb17187c7e0000c6',
      title: '生成题目所示的蛇形数组',
      motoTitle: '',
      kyu: 3,
      reporter: Users.Cyberpunkjill,
      reportedTime: new Date(2021, 10, 15, 18, 6),
      sakamotos: [Users.Cyberpunkjill, Users.Eki],
    },
    {
      id: '600bfda8a4982600271d6069',
      title: '生成指定序数的回文数字',
      motoTitle: '',
      kyu: 6,
      reporter: Users.Eki,
      reportedTime: new Date(2021, 10, 19, 10, 43),
      reviewID: '600bfdbd6c0c2e0001119a87',
      coolSolutions: ['60759041aa08ad0001f0041d'],
      sakamotos: [Users.Eki],
    },
    {
      id: '600c18ec9f033b0008d55eec',
      title: '生成极大指定序数的回文数字',
      motoTitle: '',
      kyu: 4,
      reporter: Users.Eki,
      reportedTime: new Date(2021, 10, 19, 10, 43),
      sakamotos: [Users.Eki],
    },
    {
      id: '615e5e9b9a7e1f0051199cb9',
      title: '正则匹配ascii特殊字符，限定代码长度',
      motoTitle: '',
      kyu: 6,
      reporter: Users.Eki,
      reportedTime: new Date(2021, 10, 19, 17, 24),
      sakamotos: [Users.Eki],
    },
    {
      id: '541c8630095125aba6000c00',
      title: '求平方数的数位和根，经典的数学思维',
      motoTitle: '',
      kyu: 6,
      reporter: Users.Eki,
      reportedTime: new Date(2021, 10, 19, 18, 0),
      sakamotos: [Users.Eki],
    },
    {
      id: '5d09430767455c0019eb5a3f',
      title: '异规则数列',
      motoTitle: '',
      kyu: 6,
      reporter: Users.Eki,
      reportedTime: new Date(2021, 10, 20, 11, 16),
      sakamotos: [Users.Eki],
    },
    {
      id: '556206664efbe6376700005c',
      title: '转换异规则可被除数，练习简单的进制转换算法',
      motoTitle: '',
      kyu: 4,
      reporter: Users.Eki,
      reportedTime: new Date(2021, 10, 20, 17, 35),
      sakamotos: [Users.Eki],
    },
    {
      id: '52608f5345d4a19bed000b31',
      title: '数字转换为中文表达',
      motoTitle: '',
      kyu: 4,
      reporter: Users.Eki,
      reportedTime: new Date(2021, 10, 21, 8, 56),
      sakamotos: [Users.Eki],
    },
    {
      id: '540afbe2dc9f615d5e000425',
      title: '验证数独有效性',
      motoTitle: '',
      kyu: 4,
      reporter: Users.Eki,
      reportedTime: new Date(2021, 10, 21, 9, 51),
      sakamotos: [Users.Eki],
    },
    {
      id: '55c4eb777e07c13528000021',
      title: '阶乘尾0个数，要求支持不同进制',
      motoTitle: '',
      kyu: 4,
      reporter: Users.Eki,
      reportedTime: new Date(2021, 10, 21, 15, 28),
      sakamotos: [Users.Eki],
    },
    {
      id: '53f40dff5f9d31b813000774',
      title: '根据元组字符推断单词，本质是三消游戏',
      motoTitle: '',
      kyu: 4,
      reporter: Users.Eki,
      reportedTime: new Date(2021, 10, 22, 9, 1),
      sakamotos: [Users.Eki],
    },
    {
      id: '55f91566e0d2317066000057',
      title: '函数柯里化',
      motoTitle: '',
      kyu: 4,
      reporter: Users.Eki,
      reportedTime: new Date(2021, 10, 22, 10, 26),
    },
  ];
};
