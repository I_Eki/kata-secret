<script setup>
import Kata from '../components/Kata.vue'
</script>

# 2 Kyu

## 用生成器来连续产生质数（2500万+/s）

<Kata id="prime-streaming-nc-17" />
