<script setup>
import Kata from '../components/Kata.vue'
</script>

# 6 Kyu

## 生成指定序数的回文数字

<Kata id="600bfda8a4982600271d6069" />

## 正则匹配ascii特殊字符，限定代码长度

<Kata id="615e5e9b9a7e1f0051199cb9" />

## 求平方数的数位和根，经典的数学思维

<Kata id="541c8630095125aba6000c00" />

## 异规则数列

<Kata id="5d09430767455c0019eb5a3f" />
